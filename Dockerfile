FROM ghcr.io/linuxserver/jellyfin

RUN set -ex; \
    env DEBIAN_FRONTEND=noninteractive apt -y update; \
    env DEBIAN_FRONTEND=noninteractive apt -y --no-install-recommends full-upgrade; \
    env DEBIAN_FRONTEND=noninteractive apt -y --no-install-recommends install vainfo; \
    env DEBIAN_FRONTEND=noninteractive apt -y clean; \
    rm -rf /var/cache/apt/archives/*

RUN set -ex; \
    curl -fLSso /tmp/jellyfin-ffmpeg.deb 'https://repo.jellyfin.org/releases/server/ubuntu/versions/jellyfin-ffmpeg/4.3.2-1/jellyfin-ffmpeg_4.3.2-1-focal_amd64.deb'; \
    dpkg -i /tmp/jellyfin-ffmpeg.deb; \
    rm -f /tmp/jellyfin-ffmpeg.deb; \
    ln -f /usr/lib/x86_64-linux-gnu/libva* /usr/lib/jellyfin-ffmpeg/lib/

COPY 20-intel-first /etc/cont-init.d/
